public class ControleRemoto {

    Integer canal = 130; // valor inicial
    Integer volume = 20; // valor inicial

    Imprime tela = new Imprime();

    // método para alteração de canal
    void mudarCanal(Integer novoCanal){
        if (canal.equals(novoCanal)){
            tela.imprime("Esse é o canal atual: " + canal);
        } else {
            canal = novoCanal;
            tela.imprime("Canal alterado para " + canal);
        }
    }

    // método para alteração do volume
    void mudarVolume(Integer novoVolume){
        if(volume.equals(novoVolume)){
            tela.imprime("Volume atual: " + volume);
        } else {
            volume = novoVolume;
            tela.imprime("Volume alterado para: " + volume);
        }
    }
}

