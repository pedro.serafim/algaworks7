import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        ControleRemoto controle = new ControleRemoto();
        Imprime tela = new Imprime();
        Scanner entrada = new Scanner(System.in);

        Integer canalDoUsuario;
        Integer volumeDoUsuario;

        tela.imprime("Entre com o canal desejado: ");
        canalDoUsuario = entrada.nextInt();
        tela.imprime("Entre com o volume: ");
        volumeDoUsuario = entrada.nextInt();

        controle.mudarCanal(canalDoUsuario);

        controle.mudarVolume(volumeDoUsuario);
    }
}

